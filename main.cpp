#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeexception.h"
#include <array>
#include "pentagon.h"
#include "hexagon.h"

void reInput(void);
void moreThanOneChar(char * c);

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, mem = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	pentagon pen(nam , col, mem);
	hexagon hex(nam, col, mem);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpen = &pen;
	Shape *ptrhex = &hex;

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p';
	char * shapetype = new char;
	std::string x = "y";
	int warning = false;
	while (x != "x")
	{
		std::cout << "which shape would you like to work with?.. \nc = circle, q = quadrilateral, r = rectangle, p = parallelogram, g = pentagon, h = hexagon" << std::endl;
		std::cin >> shapetype;
		try
		{
			moreThanOneChar(shapetype);
		}
		catch (moreThanOneCharException &e)
		{
			std::cout << e.moreThanOne() << std::endl;
			warning = true;
			continue;
		}
		if (warning)//==True
		{
			std::cout << "(\"Warning - Don't try to build more than one shape at once\")" << std::endl;
			warning = false;
		}
		try
		{
			switch (shapetype[0])
			{
			case 'c':
				while (std::cout << "enter color, name, rad for circle" << std::endl && !(std::cin >> col >> nam >> rad))
				{
					reInput();
				}
				circ.setColor(col);
				circ.setName(nam);
				try {
					circ.setRad(rad);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				ptrcirc->draw();
				break;
			case 'q':
				while (std::cout << "enter name, color, height, width" << std::endl && !(std::cin >> nam >> col >> height >> width))
				{
					reInput();
				}
				quad.setName(nam);
				quad.setColor(col);
				try {
					quad.setHeight(height);
					quad.setWidth(width);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				ptrquad->draw();
				break;
			case 'r':
				while (std::cout << "enter name, color, height, width" << std::endl && !(std::cin >> nam >> col >> height >> width))
				{
					reInput();
				}
				rec.setName(nam);
				rec.setColor(col);
				try {
					rec.setHeight(height);
					rec.setWidth(width);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				ptrrec->draw();
				break;
			case 'p':
				while (std::cout << "enter name, color, height, width, 2 angles" << std::endl && !(std::cin >> nam >> col >> height >> width >> ang >> ang2))
				{
					reInput();
				}
				para.setName(nam);
				para.setColor(col);
				try {
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(ang, ang2);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				catch (parallelogramValidAnglesException &e)
				{
					std::cout << e.parallelogramAngles() << std::endl;
				}
				ptrpara->draw();
				break;
			case 'g':
				while (std::cout << "enter name, color, member" << std::endl && !(std::cin >> nam >> col >> mem))
				{
					reInput();
				}
				pen.setName(nam);
				pen.setColor(col);
				try {
					pen.setMember(mem);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				ptrpen->draw();
				break;
			case 'h':
				while (std::cout << "enter name, color, member" << std::endl && !(std::cin >> nam >> col >> mem))
				{
					reInput();
				}
				hex.setName(nam);
				hex.setColor(col);
				try {
					hex.setMember(mem);
				}
				catch (negativeValueException &e)
				{
					std::cout << e.negativeValue() << std::endl;
					break;
				}
				ptrhex->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				continue;
			}
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (std::exception e)
		{
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}
	system("pause");
	return 0;
}

void reInput(void)
{
	std::cin.clear(); //clear bad input flag
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //discard input
	std::cout << "Invalid input, please enter again." << std::endl;
}

void moreThanOneChar(char * c)
{
	if (strlen(c) != 1)
		throw moreThanOneCharException();
}