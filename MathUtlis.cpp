#include "MathUtlis.h"

MathUtlis::MathUtlis()
{
}

MathUtlis::~MathUtlis()
{
}

double MathUtlis::calQuadParimeter(double h, double w)
{
	return 2 * (w + h);
}

double MathUtlis::calArea(double h, double w)
{
	return h * w;
}

double MathUtlis::calCircleCircumference(double rad)
{
	return 2 * (3.14)*rad;
}

double MathUtlis::calCircleArea(double rad)
{
	return 3.14*rad*rad;;
}

double MathUtlis::calPentagonArea(double a)
{
	return (sqrt(5 * (5 + 2 * (sqrt(5)))) * a * a) / 4;
}

double MathUtlis::calHexagonArea(double a)
{
	return ((3 * sqrt(3) * (a * a)) / 2);
}
 