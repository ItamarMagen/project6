#pragma once
#include <math.h>
#ifndef MATHUTLIS_H
#define MATHUTLIS_H

class MathUtlis
{
public:
	MathUtlis();
	~MathUtlis();
	static double calQuadParimeter(double h, double w);
	static double calArea(double h, double w);
	static double calCircleCircumference(double rad);
	static double calCircleArea(double rad);
	static double calPentagonArea(double a);
	static double calHexagonArea(double a);
};
#endif
