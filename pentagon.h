#pragma once

#ifndef PENTAGON_H
#define PENTAGON_H
#include "shape.h"
#include "MathUtlis.h"
#include <iostream>

class pentagon: public Shape
{
public:
	void draw();
	void setMember(int m);
	pentagon(std::string, std::string, double);
	double calPerimater();
	double calArea();
	double getMember();
private:
	double member;
};
#endif