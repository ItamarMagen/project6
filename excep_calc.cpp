#include <iostream>
#define PERMITTED_VALUE 8200

int add(int a, int b)
{
	if (a + b == PERMITTED_VALUE)
		throw(std::string("ERROR"));
	return a + b;
}

int multiply(int a, int b)
{
	int sum = 0;
	for (int i = 0; i < b; i++)
	{
		try {
			sum = add(sum, a);
		}
		catch (std::string e)
		{
			throw(std::string("ERROR"));
		}
	}
	return sum;
}

int pow(int a, int b) 
{
	int exponent = 1;
	if (a == PERMITTED_VALUE || b == PERMITTED_VALUE)
		throw(std::string("ERROR"));
	for (int i = 0; i < b; i++)
	{
		try {
			exponent = multiply(exponent, a);
		}
		catch (std::string e)
		{
			throw(std::string("ERROR"));
		}
	}
	return exponent;
}

int main(void) 
{
	try {
		std::cout << pow(PERMITTED_VALUE, 1) << std::endl;
	}
	catch(std::string e)
	{
		std::cout << "This user is not authorized to access 8200,"
			<< "please enter different numbers, or try to get clearance in 1 year." << std::endl;
	}
	system("PAUSE");
}