#include "pentagon.h"
#include "shapeexception.h"
#include "MathUtlis.h"

void pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "member is " << getMember() <<  std::endl << "Area is " << calArea() << std::endl << "Perimeter is " << calPerimater() << std::endl;
}

void pentagon::setMember(int m)
{
	if (m < 0)
		throw negativeValueException();
	member = m;
}

pentagon::pentagon(std::string name, std::string color, double member):Shape(name, color)
{
	setMember(member);
}

double pentagon::calPerimater()
{
	return member * 5;
}

double pentagon::calArea()
{
	return MathUtlis::calPentagonArea(member);
}

double pentagon::getMember()
{
	return member;
}
