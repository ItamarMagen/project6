#pragma once

#include "shape.h"
#include "MathUtlis.h"
#include <iostream>

class hexagon : public Shape
{
public:
	void draw();
	void setMember(int m);
	hexagon(std::string, std::string, double);
	double calPerimater();
	double calArea();
	double getMember();
private:
	double member;
};