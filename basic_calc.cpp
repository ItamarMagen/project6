#include <iostream>
#define ERROR_CODE -1
#define PERMITTED_VALUE 8200
#define MAX_INPUT_VALUE 9

int add(int a, int b, int &sum) 
{
	if (a == PERMITTED_VALUE || b == PERMITTED_VALUE)
		return ERROR_CODE;
	sum = a + b;
	return 0;
}

int multiply(int a, int b, int &exponent) 
{
	int sum = 0;
	for (int i = 0; i < b; i++)
	{
		if (sum == PERMITTED_VALUE || add(sum, a, sum) == ERROR_CODE)
			return ERROR_CODE;
	}
	exponent = sum;
	return 0;
}

int pow(int a, int b, int & result) 
{
	int exponent = 1;
	if (a == PERMITTED_VALUE || b == PERMITTED_VALUE)
		return ERROR_CODE;
	for (int i = 0; i < b; i++) 
	{
		if (multiply(exponent, a, exponent) == ERROR_CODE || exponent == PERMITTED_VALUE)
			return ERROR_CODE;
	}
	result = exponent;
	return 0;
}

int main(void) 
{
	int result = 0;
	for (int i = 1; i <= MAX_INPUT_VALUE; i++)
	{
		for (int j = 1; j <= MAX_INPUT_VALUE; j++)
		{	
			std::cout << "The pow of " << i << " and " << j << " is: ";
			if (pow(i, j, result) == ERROR_CODE)
			{
				std::cout << "This user is not authorized to access 8200,"
					<< "please enter different numbers, or try to get clearance in 1 year." << std::endl;
			}
			else
				std::cout << result << std::endl;
			result = 0;
		}
		std::cout << '\n';
	}
	system("PAUSE");
}