#pragma once
#include <exception>
#include <string>

class shapeException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "This is a shape exception!";
	}
};

class negativeValueException
{
public:
	const std::string negativeValue() const
	{
		return "You put a negative value";
	}
};

class moreThanOneCharException
{
public:
	const std::string moreThanOne() const
	{
		return "Please enter only one char!";
	}
};

class parallelogramValidAnglesException
{
public:
	const std::string parallelogramAngles() const
	{
		return "Please enter only angles that the sum of them isn't over 180 (right angle).";
	}
};