#include "hexagon.h"
#include "MathUtlis.h"
#include"shapeexception.h"

void hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "member is " << getMember() << std::endl << "Area is " << calArea() << std::endl << "Perimeter is " << calPerimater() << std::endl;
}

void hexagon::setMember(int m)
{
	if (m < 0)
		throw negativeValueException();
	member = m;
}

hexagon::hexagon(std::string name, std::string color, double member) : Shape(name, color)
{
	setMember(member);
}

double hexagon::calPerimater()
{
	return member * 6;
}

double hexagon::calArea()
{
	return MathUtlis::calHexagonArea(member);
}

double hexagon::getMember()
{
	return member;
}
