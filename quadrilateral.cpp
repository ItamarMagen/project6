#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>
#include "MathUtlis.h"

//quadrilateral::quadrilateral() {}
quadrilateral::quadrilateral(std::string nam, std::string col, int h, int w) :Shape(nam, col) {

	//void setName(string nam); //PRIVATE DATA
	//void setColor(string col);
	setHeight(h);
	setWidth(w);

}
void quadrilateral::draw()
{
	//quadrilateral p;
	std::cout << getName() << std::endl << getColor() << std::endl << "Width is " << getWidth() << std::endl << "Height is " << getHeight() << std::endl << "Area is " << CalArea() << std::endl << "Perimeter is " << getCalPerimater() << std::endl;
	//cout << name << color;
}

double quadrilateral::CalArea()
{
	if (width < 0 || height < 0)
	{
		throw negativeValueException();
	}
	return MathUtlis::calArea(width, height); //RECTANGLE 
}

void quadrilateral::setHeight(int h) {
	if (h < 0)
		throw negativeValueException();
	height = h;
}
void quadrilateral::setWidth(int w) {
	if (w < 0)
		throw negativeValueException();
	width = w;
}
double quadrilateral::CalPerimater() {
	return MathUtlis::calQuadParimeter(height, width);
}
double quadrilateral::getCalPerimater() {
	return MathUtlis::calQuadParimeter(height, width);
}
int quadrilateral::getHeight() {
	return height;
}
int quadrilateral::getWidth() {
	return width;
}